
#include <iostream>
#include <string>
int main()
{
    //Enter Name
    std::cout << "Enter Your First Name: ";
    std::string FirstName;
    std::getline(std::cin, FirstName);

    //Enter Surname
    std::cout << "Enter Your Last Name: ";
    std::string LastName;
    std::getline(std::cin, LastName);
    
    //Enter Age
    std::cout << "Enter Your Age: ";
    std::string Age;
    std::getline(std::cin, Age);
        
    // Concatenating Name & Surname
    std::string FullName{ FirstName + " " + LastName};

    // Print Fullname & Age
    std::cout << "Your Full Name is " << FullName << " ";
    int num1 = stoi(Age);
    if (num1 < 10)
    {
        std::cout << "You are Baby"<< " (Aged:" << num1 << ")"<< "\n\n";
    }
    else if (num1 < 20)
    {
        std::cout << "You are Teen" << " (Aged:" << num1 << ")" << "\n\n";
    }
    else if (num1 < 55)
    {
        std::cout << "You are Adult" << " (Aged:" << num1 << ")" << "\n\n";
    }
    else std::cout << "You are Old" << " (Aged:" << num1 << ")" << "\n\n";

    //print length, 1st, last character of Name
    std::cout << "Your First Name Consists of " << FirstName.length() << " Characters" << "\n";
    std::cout << "Your First Name Starts With Character '" << FirstName.front() << "'" << "And Ends With '"  << FirstName.back() << "\n";

    //print length, 1st, last character of Surname
    std::cout << "Your Last Name Consists of " << LastName.length() << " Characters" << "\n";
    std::cout << "Your Last Name Starts With Character '" << LastName.front() << "'" << "And Ends With '" << LastName.back() << "\n";
    //print length OF fullname
    std::cout << "Your Full Name Consists of " << FirstName.length() + LastName.length() << " Characters" << "\n";
       
}

